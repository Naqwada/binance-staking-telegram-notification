# -*- coding: utf-8 -*-

# Author:   Naqwada (RuptureFarm 1029) <naqwada@pm.me>
# License:  MIT License (http://www.opensource.org/licenses/mit-license.php)
# Docs:     https://github.com/Nwqda/Binance-Staking-Telegram-Notification
# Website:  http://samy.link/
# Linkedin: https://www.linkedin.com/in/samy-younsi/
# Note:     FOR EDUCATIONAL PURPOSE ONLY.

from __future__ import print_function, unicode_literals
from datetime import datetime, timedelta
import asyncio
import httpx
import yaml
import json
import time
import os


def banner():
  BinanceStatkingLogo = """                                                    
██████╗░██╗███╗░░██╗░█████╗░███╗░░██╗░█████╗░███████╗
██╔══██╗██║████╗░██║██╔══██╗████╗░██║██╔══██╗██╔════╝
██████╦╝██║██╔██╗██║███████║██╔██╗██║██║░░╚═╝█████╗░░
██╔══██╗██║██║╚████║██╔══██║██║╚████║██║░░██╗██╔══╝░░
██████╦╝██║██║░╚███║██║░░██║██║░╚███║╚█████╔╝███████╗
╚═════╝░╚═╝╚═╝░░╚══╝╚═╝░░╚═╝╚═╝░░╚══╝░╚════╝░╚══════╝
░██████╗████████╗░█████╗░██╗░░██╗██╗███╗░░██╗░██████╗░
██╔════╝╚══██╔══╝██╔══██╗██║░██╔╝██║████╗░██║██╔════╝░
╚█████╗░░░░██║░░░███████║█████═╝░██║██╔██╗██║██║░░██╗░
░╚═══██╗░░░██║░░░██╔══██║██╔═██╗░██║██║╚████║██║░░╚██╗
██████╔╝░░░██║░░░██║░░██║██║░╚██╗██║██║░╚███║╚██████╔╝
╚═════╝░░░░╚═╝░░░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝╚═╝░░╚══╝░╚═════╝░
████████╗███████╗██╗░░░░░███████╗░██████╗░██████╗░░█████╗░███╗░░░███╗
╚══██╔══╝██╔════╝██║░░░░░██╔════╝██╔════╝░██╔══██╗██╔══██╗████╗░████║
░░░██║░░░█████╗░░██║░░░░░█████╗░░██║░░██╗░██████╔╝███████║██╔████╔██║
░░░██║░░░██╔══╝░░██║░░░░░██╔══╝░░██║░░╚██╗██╔══██╗██╔══██║██║╚██╔╝██║
░░░██║░░░███████╗███████╗███████╗╚██████╔╝██║░░██║██║░░██║██║░╚═╝░██║
░░░╚═╝░░░╚══════╝╚══════╝╚══════╝░╚═════╝░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░░░░╚═╝

      \033[1;93mBinance Locked Staking Telegram Notification\033[1;m                  

\033[1;97mAuthor:\033[1;m   \033[1;92mNaqwada (RuptureFarm 1029)\033[1;m
\033[1;97mDocs:\033[1;m     \033[1;92mhttps://github.com/Nwqda/Binance-Staking-Telegram-Notification\033[1;m
\033[1;97mLicense:\033[1;m  \033[1;92mMIT License (http://www.opensource.org/licenses/mit-license.php)\033[1;m
\033[1;97mWebsite:\033[1;m  \033[1;92mhttp://samy.link/\033[1;m      
  """
  return print('\033[1;94m{}\033[1;m'.format(BinanceStatkingLogo))


async def get_staking_assets_list():
  endpoint = 'https://www.binance.com/gateway-api/v1/friendly/pos/union?pageSize=100&pageIndex=1&status=ALL'
  async with httpx.AsyncClient() as client:
    response = await client.get(endpoint)
    return response.json()['data']


def load_config_vars(filename):
  config_file = os.path.isfile(filename) 
  if not config_file:
    print("\033[1;93mThe configuration file \"{}\" cannot be found.\033[1;m For more information please consult the documentation on GitHub (https://github.com/Nwqda/Binance-Staking-Telegram-Notification).".format(filename))

  with open('config.yml', 'r') as file:
    data = yaml.load(file, Loader=yaml.loader.SafeLoader)
    return data


def check_wished_assets_available(tracked_assets, staking_assets_list):
  wished_assets = {}
  for staking_asset in staking_assets_list:
    for index, token in enumerate(tracked_assets):
      if tracked_assets[token]['name'] == staking_asset['asset']:
        wished_assets[index] = staking_asset
  return wished_assets


def check_available_assets_duration(tracked_assets, wished_assets):
  available_assets = []
  for index, wished_asset in enumerate(wished_assets):
    for index, project in enumerate(wished_assets[index]['projects']):
      for index, token in enumerate(tracked_assets):
        if tracked_assets[token]['name'] == project['asset']:
          wished_durations = tracked_assets[token]['duration'].split(',')
          if project['duration'] in wished_durations and project['sellOut'] == False:
            available_assets.append('{}: {}days'.format(project['asset'], project['duration']))
  return available_assets


def get_last_notification(filename):
  last_notification_file = os.path.isfile(filename) 
  if not last_notification_file:
    last_notification_file = open(filename, 'w')
    last_notification_file.write(str(datetime.now() - timedelta(minutes = 10)))
    last_notification_file.close()

  last_notification_file = open(filename, 'r')
  return last_notification_file.read()


def set_last_notification(filename):
  last_notification_file = open(filename, 'w+')
  last_notification_file.write(str(datetime.now()))
  last_notification_file.close()


def send_telegram_notification(telegram_credential, available_assets):
  filename = 'last_notification.txt'
  last_notification = get_last_notification(filename)

  if (datetime.strptime(last_notification, '%Y-%m-%d %H:%M:%S.%f') + timedelta(minutes = 10)) < datetime.now():
    message_text = "<b>New Asset(s) available:</b>\n\n{}\n".format("\n".join(available_assets))
    url = 'https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}&parse_mode=html'.format(telegram_credential['apiToken'], telegram_credential['chatID'], message_text)
    response = httpx.get(url)
    print("\033[1;93m[{}]\033[1;m Telegram message sent:\n{}".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S"), message_text))
    set_last_notification(filename)


async def main():
  banner()
  filename = 'config.yml'
  config = load_config_vars(filename)
  
  execution_time = config['execution_time']

  print("\033[1;93m[+] Your configuration:\033[1;m\nBot Execution:  Every {} seconds\nTracked Assets:\n{}\n".format(execution_time, "\n".join("{} ({})days".format(config['tracked_assets'][tracked_asset]['name'], config['tracked_assets'][tracked_asset]['duration']) for index, tracked_asset in enumerate(config['tracked_assets']))))
  print("\033[1;93m[{}]\033[1;m Waiting for opportunities... Be patient.".format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))

  while True:
   staking_assets_list = await get_staking_assets_list()
   wished_assets = check_wished_assets_available(config['tracked_assets'], staking_assets_list)
   available_assets = check_available_assets_duration(config['tracked_assets'], wished_assets)
   if available_assets:
    send_telegram_notification(config['telegram_credential'], available_assets)
   time.sleep(execution_time) 


if __name__ == '__main__':
  asyncio.run(main())