# Binance Locked Staking Telegram Notification
Binance Locked Staking Telegram Notification is a useful tool that can be used to received Telegram notification when locked staking options project of your choice become available.

![Binance Locked Staking Telegram Notification](https://i.ibb.co/875rfhw/Binance-Locked-Staking-Telegram-Notification.png)

In the configuration file, you can define multiple cryptocurrencies but also multiple duration projects that you wish to track. You can also specify how often you want the script to run (every minute, every 2 minutes, hour? etc...). When the program detects if a chosen duration project based on the currency of your choice become available then a Telegram notification will be sent every 10 minutes until the opportunity is sold out.

### Requirement
* Python 3 (Tested with Python 3.9)
* Internet connection

### Installation
Clone this repository and run:
```shell
pip install -r requirements.txt
```

### Configuration
Edit the file `config.yml` and add your Telegram API information.
```yaml
telegram_credential:
  apiToken: "<API_TOKEN>" #Insert your API_TOKEN.
  chatID: "<chatID>" #Insert your chatID.
```
Then define the currencies and duration you wish to track.
```yaml
tracked_assets:
  
  token_0:
    name: "MATIC"
    duration: "60"

  token_1:
    name: "BNB"
    duration: "60,90,120"

  token_2:
    name: "CRV"
    duration: "30,90"
```
Note: Make sure to respect the naming conventions "token_x" if you want to track other currencies. Also, separate duration values with a comma if you want to track multiple durations project for a given currency.  

#### Usage
```
python3 bstn.py
```
That's all, be patient until receiving notifications!

![Binance Locked Staking Telegram bot](https://i.ibb.co/GcNM323/Binnance-staking-bot.jpg)
### Proof of concept

[![Video PoC Binance Locked Staking Telegram Notification](https://i.ibb.co/7gXHL9q/500px-youtube-social-play.png)](https://youtu.be/KM5ZwleStPM)
